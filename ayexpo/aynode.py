"""
aynode.py is base class for file or dir type.
"""
import calendar
import datetime
import os
import pwd
import stat
import sys


class AyNode:
    """Base class for directory or file type"""

    def __init__(self, node):
        """Constructing the AyFile :param node: os.DirEntry

        Args:
            node:
        """
        self.__name = ''
        self.__path = ''
        self.__path = os.path.normpath(node.path)
        self.name = node.name
        self._is_dir = False
        self.__gen_stat()

    @staticmethod
    def node_factory(node, path=False):
        """Factory to make folder or file node.
        Args:
            node: AyNode
            path: str
        """
        if path:
            node = AyNode.__gen_node_from_path(node)
        if node.is_file():
            return AyFile(node)
        return AyFolder(node)

    @staticmethod
    def __gen_node_from_path(path):
        """
        Args:
            path: str
        """
        obj = FileAdapter()
        obj.path = path
        obj.name = os.path.basename(path)
        obj.file = os.path.isfile(path)
        return obj

    @property
    def is_dir(self):
        """
        Returns:
            bool
        """
        return self._is_dir

    @property
    def path(self):
        """
        Returns:
            str
        """
        return self.__path

    @path.setter
    def path(self, path):
        """
        Args:
            path: str
        """
        self.__path = path

    @property
    def name(self):
        """
        Returns:
            str
        """
        return self.__name

    @name.setter
    def name(self, name):
        """
        Args:
            name: str
        """
        self.__name = name

    def __gen_stat(self):
        """Generate node status from path"""
        self.__stat = os.stat(self.path)

    def is_hidden(self):
        """Check file is hidden or not :return: bool"""
        if sys.platform in ['win32', 'cygwin']:
            return self.stat.st_file_attributes & stat.FILE_ATTRIBUTE_HIDDEN
        else:
            return self.name.startswith('.')

    @property
    def stat(self):
        """Return node status :rtype: stat_result"""
        return self.__stat

    @property
    def mode(self):
        """Return node permission as unix type :rtype: str"""
        return stat.filemode(self.stat.st_mode)

    @property
    def nlink(self):
        """Return node hard link number :rtype: int"""
        return self.stat.st_nlink

    @property
    def group(self):
        """Return node group name :rtype: str"""
        return str(pwd.getpwuid(self.stat.st_gid).pw_name)

    @property
    def owner(self):
        """Return node owner name :rtype: str"""
        return str(pwd.getpwuid(self.stat.st_uid).pw_name)

    @property
    def size(self):
        """Return node size :rtype: int"""
        return self.stat.st_size

    @property
    def modified_month(self):
        """Return node modified date month name :rtype: str"""
        return calendar.month_abbr[datetime.datetime.fromtimestamp(self.stat.st_mtime).month]

    @property
    def modified_day(self):
        """Return node modified date day :rtype: int"""
        return datetime.datetime.fromtimestamp(self.stat.st_mtime).day

    @property
    def modified_time(self):
        """Return node modified time :rtype: str"""
        h = datetime.datetime.fromtimestamp(self.stat.st_mtime).hour
        m = datetime.datetime.fromtimestamp(self.stat.st_mtime).minute
        time = "{0}:".format(h)
        if h < 10:
            time = "0{0}:".format(h)
        if m < 10:
            return time + "0{0}".format(m)
        return time + "{0}".format(m)

    def rename(self, name):
        """
        Args:
            name: str
        """
        parent = os.path.abspath(os.path.join(self.path, os.pardir))
        os.rename(self.path, "{0}{1}{2}".format(parent, os.path.sep, name))


class AyFolder(AyNode):
    """AyFolder class"""

    def __init__(self, node):
        """
        Args:
            node: os.DirEntry
        """
        super().__init__(node)
        self._is_dir = True
        self.__nodes = None

    def __gen_nodes(self):
        """Generating enclosed nodes in folder"""
        self.__nodes = list()
        try:
            with os.scandir(self.path) as nodes:
                for node in nodes:
                    self.__nodes.append(AyNode.node_factory(node))
        except Exception as err:
            print(err)

    @property
    def nodes(self):
        self.__gen_nodes()
        return self.__nodes


class AyFile(AyNode):
    """AyFile class"""

    def __init__(self, node):
        """
        Args:
            node: str
        """
        super().__init__(node)


class FileAdapter:
    name = ''
    path = ''
    file = ''

    def is_file(self):
        return self.file

    def is_dir(self):
        return not self.file
