"""
view.py
"""
import abc


class View(metaclass=abc.ABCMeta):
    """View Base Class"""
    @abc.abstractmethod
    def show_error(self, err):
        """
        Args:
            err:
        """
        pass

    @abc.abstractmethod
    def show_nodes(self, nodes, hidden, recursive, search, regex, replace):
        """
        Args:
            nodes: AyNode []
            hidden: bool
            recursive: bool
            search: str
            regex: str
        """
        pass
