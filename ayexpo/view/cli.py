"""
Command Line Interface
"""

from ayexpo.view.view import View
from tabulate import tabulate
import re

class Cli(View):
    """Cli class"""
    __instance = None

    @staticmethod
    def get_instance():
        """class singleton :return: Cli"""
        if Cli.__instance is None:
            Cli.__instance = Cli()
        return Cli.__instance

    def show_error(self, err):
        """
        Args:
            err:
        """
        print("Error! {0}".format(err))

    def show(self, table):
        """
        Args:
            table: list
        """
        print(tabulate(table, tablefmt="plain", numalign="right"))

    def show_nodes(self, nodes, hidden, recursive, search, regex, replace):
        """
        Args:
            nodes: AyNode []
            hidden: bool
            recursive: bool
            search: str
            regex: str
            replace: str
        """
        rec = list()
        table = list()
        rex = None
        if regex:
            rex = re.compile(regex)
        for node in nodes:
            if not hidden and node.is_hidden():
                continue
            if node.is_dir and recursive:
                rec.append(node)
            if regex and not rex.match(node.name):
                continue
            if search and search != node.name:
                continue
            if replace:
                node.rename(replace)
            row = [node.mode, node.nlink, node.group, node.owner, node.size, node.modified_month, node.modified_day,
                   node.modified_time, node.name]
            if len(row):
                if search or regex and node.is_dir:
                    print()
                    print("{0}:".format(node.path))
                table.append(row)
        if search is None and regex is None:
            print("total {0}".format(len(table)))
        if len(table):
            self.show(table)
        for node in rec:
            if search is None and regex is None:
                print()
                print("{0}:".format(node.path))
            self.show_nodes(node.nodes, hidden, recursive, search, regex)
