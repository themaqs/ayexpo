"""
ayexpo.py
"""
from ayexpo.aynode import *
from ayexpo.view.cli import Cli


class AyExpo:
    """Explorer controller"""
    __instance = None

    @staticmethod
    def get_instance(path):
        """
        Args:
            path:
        """
        if AyExpo.__instance is None:
            AyExpo.__instance = AyExpo(path)
        return AyExpo.__instance

    def __init__(self, path):
        """
        Args:
            path:
        """
        self.__root_node = None
        if path == '':
            path = os.getcwd()
        self.__root_path = path
        self.__build_root()
        self.__view = Cli.get_instance()

    def __build_root(self):
        """Building root node"""
        self.__root_node = AyNode.node_factory(self.__root_path, path=True)

    def show_list(self, root=False, hidden=False, recursive=False, search=None, regex=None, replace=None):
        """
        Args:
            root: bool
            hidden: bool
            recursive: bool
            search: str
            regex: str
            replace: str
        """
        nodes = None
        if not self.__root_node.is_dir or (root and search is None and regex is None):
            nodes = [self.__root_node]
        else:
            nodes = self.__root_node.nodes
        if replace and search == self.__root_node.name:
            self.__root_node.rename(replace)
        return self.__view.show_nodes(nodes, hidden, recursive, search, regex, replace)
