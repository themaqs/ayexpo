"""
This script use to begin project and fetch args from cli
"""

import argparse
from ayexpo.view.cli import Cli
from ayexpo.ayexpo import AyExpo

EMPTY = '!%empty%!'
DESCRIPTION = 'Ayexpo - simple file explorer'


def parse_arguments():
    """Parse and process user arguments :rtype: ArgumentParser"""
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('path', nargs='?', default=EMPTY, help='File or directory path.')
    parser.add_argument('-l', help='List all sub nodes (file or directory) in the path.', action='store_const',
                        const=True, default=False)
    parser.add_argument('-a', help='Flag true to operate the hidden files or directories.',
                        action='store_const', const=True, default=False)
    parser.add_argument('-r', help='Flag true to operate recursively',
                        action='store_const', const=True, default=False)
    parser.add_argument('-s', help='Search a node (file or directory) in the path.')
    parser.add_argument('-g', help='Search a node with regex (file or directory) in the path.')
    parser.add_argument('-b', help='Replace a node (file or directory) in the path.')
    return parser.parse_args()


def do_op(args, app):
    """
    Args:
        args: arg_parser
        app: AyExpo
    """
    app.show_list(root=(not args.l), hidden=args.a, recursive=args.r, regex=args.g, search=args.s, replace=args.b)


def run():
    """Running the project :rtype: object"""
    view = Cli.get_instance()
    try:
        args = parse_arguments()
        path = args.path
        if path == EMPTY:
            path = ''
        app = AyExpo.get_instance(path)
        do_op(args, app)
    except Exception as err:
        Cli.get_instance().show_error(err)


if __name__ == '__main__':
    run()
