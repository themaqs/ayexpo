import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="ayexpo-themaqs",
    version="0.0.1",
    author="Aydin Mohandesi",
    author_email="aydin.mohandesi",
    description="A small file explorer utils for python.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/themaqs/pyexplorer",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GENERAL PUBLIC LICENSE V3 (GPLV3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
